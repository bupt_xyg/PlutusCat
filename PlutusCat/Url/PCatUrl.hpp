/*
 * @Author: CopyRight@ xyg[2369941198@qq.com] 
 * @Date: 2021-03-28 15:31:59 
 * @Last Modified by: xyg[2369941198@qq.com]
 * @Last Modified time: 2021-03-28 16:21:57
 */
// PCatUrl Module
// Distributed under the MIT License
// This file is the complete reference of URL Module
// Generally , this Module could not be imported into your own project separately.
// But , there is no coupling between this module and others, 
// that means you can import this module separately by copy this hpp-file to your own project folder, 
// then you can import it by this statements : #include <folder\PCatUrl.hpp> or #include "folder\PCatUrl.hpp"
#ifndef PCAT_PCATURL_H
#define PCAT_PCATURL_H
#include "libs\assert.hpp"
#include <string>
namespace plutu_cat { 
namespace Url {
    //protocol :// hostname[:port] / path / [;parameters][?query]#fragment
using URL_STRING = const std::string*;
using URL_MODE_CODE = const std::int32_t;
using URL_PORT = std::int32_t;
class Url
{
    URL_MODE_CODE Silent            = 0;
    URL_MODE_CODE ImportantOnly     = 1;
    URL_MODE_CODE MuteCheckingMsg   = 2;
    public:

        Url(
            URL_STRING url_string = NULL
            )
            :_url_text(url_string),
            _url_protocol(NULL),
            _url_hostname(NULL),
            _url_port(0),
            _url_filepath(NULL),
            _url_query(NULL),
            _url_frag(NULL)
            { 
                _validity = check_validity_Url();
                EncodeUrl();
            } 

        Url& operator= (const Url& other)
        { 
            _url_text = other._url_text;
            _validity = other._validity;
            return *this;
        }

        Url& SetUrl(URL_STRING url_string) 
        {
            _url_text = _url_text;
            _validity = check_validity_Url();
            EncodeUrl();
        }
        
        URL_STRING     get_protocol() {}
        URL_STRING     get_hostname() {}
        URL_PORT       get_port() {}
        URL_STRING     get_filepath() {}
        URL_STRING     get_query() {}
        URL_STRING     get_frag() {}

        Url&            to_json(){}
        Url&            open(){}
        Url&            warningMode(URL_MODE_CODE mode) {}
    private:

        URL_STRING _url_text;
        URL_STRING _url_protocol;
        URL_STRING _url_hostname;
        URL_STRING _url_filepath;
        URL_STRING _url_query;
        URL_STRING _url_frag;
        URL_PORT _url_port;
        bool _validity;
        bool check_validity_Url(){};
        void EncodeUrl(){};

};
}//namespace plutu_cat
}//namespace Url
#endif

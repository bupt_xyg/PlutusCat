# PlutuCat

## 目录

[TOC]



## 开发情况
### 功能情况
* **csv文件io支持** [✔]
* **dataframe构造(construct)** [✔]
* **dataframe修改** [待完成]
* 1. *删除* [待完成]
* 2. *修改* [待完成]
* 3. *增加* [待完成]
* 4. *合并* [待完成]
* 5. *去重* [待完成]
* 6. *基础查找* [完成]
* **dataframe↓查找拓展(AVL_p)** [待完成]
* 1. AVL树形构造 [待完成]
* 2. AVL操作 [待完成]
* **dataframe↓匹配拓展(match_p)** [完成]
* 1. [表达式计算](/docs/match_p_api.mdmatch_p_api.md) [完成]
* 2. [match接口](/docs/match_p_api.mdmatch_p_api.md) [完成]
* 3. [eval接口](/docs/expr_parse_api.mdexpr_parse_api.md) [完成]
* **dataframe↓HASH拓展(hash_p)** [待完成]
* 1. hash构造 [未完成]
* 2. 冲突调解 [未完成]
* **dataframe统计拓展(statu_p)** [待完成]
* * **数值型**
* * 1. 求和 [完成]
* * 2. mean [完成]
* * 3. median [待完成]
* * 4. sortby [待完成]
* * 5. bp [待完成]
* * 6. ...
* * **数学型**
* * 1. 高精度运算 [待完成]
* * 2. ...
* ...
### 开发规范


### 开发预期
## 使用方法

### 欢迎使用PlutuCat

###  Getting started

#### With Windows

#### With linux

#####

### Built
There are serval ways to incorporate Plutu into your projects



## 开源声明

MIT License

>  Copyright (c) 2009-2021 GuoZi
>
>  Permission is hereby granted, free of charge, to any person obtaining a copy
>  of this software and associated documentation files (the "Software"), to deal
>  in the Software without restriction, including without limitation the rights
>  to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
>  copies of the Software, and to permit persons to whom the Software is
>  furnished to do so, subject to the following conditions:
>
>  The above copyright notice and this permission notice shall be included in
>  all copies or substantial portions of the Software.
>
>  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
>  IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
>  FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
>  AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
>  LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
>  OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
>  THE SOFTWARE.

## 帮助
如果在使用PlutuCat的过程中遇到了问题，您可以在此处或issue中寻找解决方法

- [小鱼干](https://gitee.com/)
- [KingHorin](https://gitee.com/notifications/messages/)
